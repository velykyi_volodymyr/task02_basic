/**
 * This is a part of task02 for online Epam Java Course.
 */
package com.velykyi.numbers;

/**
 * We will be using to do some actions with Fibonacci numbers.
 *
 * @author Volodymyr Velykyi
 */
public class Fibonacci {

    /**
     * Create a sequence of Fibonacci numbers and put it in array.
     *
     * @param n is count of Fibonacci numbers.
     * @param F1 is first number of sequence.
     * @param F2 is second number of sequence.
     * @return array of Fibonacci numbers.
     */
    public static int[] createNumbers(int n, int F1, int F2) {
        int[] fibonacciNumb = new int[n];
        fibonacciNumb[0] = F1;
        fibonacciNumb[1] = F2;
        System.out.print("Fibonacci numbers: " + F1 + " " + F2 + " ");
        for (int i = 2; i < n; i++) {
            fibonacciNumb[i] = fibonacciNumb[i - 1] + fibonacciNumb[i - 2];
            System.out.print(fibonacciNumb[i] + " ");
        }
        return fibonacciNumb;
    }

    /**
     * Count percentage od odd and even numbers.
     *
     * @param array is sequence of numbers.
     */
    public static void countPercentage(int[] array) {
        int oddCounter;
        int evenCounter;
        double percentageOdd;
        double percentageEven;
        oddCounter = Number.countOdd(array);
        evenCounter = Number.countEven(array);
        percentageOdd = (oddCounter / (double) array.length) * 100;
        percentageEven = (evenCounter / (double) array.length) * 100;
        System.out.println("\nPercentage of odd numbers: " + percentageOdd);
        System.out.println("Percentage of even numbers: " + percentageEven);
    }

    /**
     * Print the biggest odd number from array.
     *
     * @param numbers is array of Fibonacci numbers.
     */
    public static void printBiggestOdd(int[] numbers) {
        Number.printBiggestOdd(numbers);
    }

    /**
     * Print the biggest even number from array.
     *
     * @param numbers is array of Fibonacci numbers.
     */
    public static void printBiggestEven(int[] numbers) {
        Number.printBiggestEven(numbers);
    }
}
