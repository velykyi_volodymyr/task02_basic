/**
 * This is a part of task02 for online Epam Java Course.
 */
package com.velykyi.numbers;

/**
 * We will using to work with numbers.
 *
 * @author Volodymyr Velykyi
 */
public class Number {

    /**
     * Print odd numbers in the interval.
     *
     * @param interval is interval which was given by user.
     * @return maximal odd number.
     */
    public static int printOdd(int[] interval) {
        System.out.println("\nOdd numbers:");
        int i = interval[0];
        int sum = 0;
        int maxOdd = 0;
        while (i <= interval[1]) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
                maxOdd = i;
                sum += i;
            }
            i++;
        }
        System.out.println("Sum of odd numbers = " + sum);
        return maxOdd;
    }

    /**
     * Print even numbers in the interval.
     *
     * @param interval is interval which was given by user.
     * @return maximal even number.
     */
    public static int printEven(int[] interval) {
        System.out.println("\nEven numbers:");
        int i = interval[1];
        int sum = 0;
        int maxEven = 0;
        while (i >= interval[0]) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                sum += i;
                if (maxEven < i) {
                    maxEven = i;
                }
            }
            i--;
        }
        System.out.println("Sum of even numbers = " + sum);
        return maxEven;
    }

    /**
     * Count odd numbers in sequence.
     *
     * @param array is sequence of number
     * @return amount of odd numbers
     */
    protected static int countOdd(int[] array) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 != 0) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Count even numbers in sequence.
     *
     * @param array is sequence of number
     * @return amount of even numbers
     */
    protected static int countEven(int[] array) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Print the biggest odd numbers in the array.
     *
     * @param array is sequence of numbers.
     */
    protected static void printBiggestOdd(int[] array) {
        System.out.println("\nThe biggest odd number: ");
        int i = array.length - 1;
        while (i >= 0) {
            if (array[i] % 2 != 0) {
                System.out.print(array[i]);
                break;
            }
            i--;
        }
    }

    /**
     * Print the biggest even numbers in the array.
     *
     * @param array is sequence of numbers.
     */
    protected static void printBiggestEven(int[] array) {
        System.out.println("\nThe biggest even number: ");
        int i = array.length - 1;
        while (i >= 0) {
            if (array[i] % 2 == 0) {
                System.out.print(array[i]);
                break;
            }
            i--;
        }
    }
}
