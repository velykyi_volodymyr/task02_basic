/**
 * This is task02 for online Epam Java Course.
 */
package com.velykyi.application;

import com.velykyi.numbers.Fibonacci;
import com.velykyi.numbers.Number;

import java.util.Scanner;

/**
 * We will be using Application to call methods from other classes.
 *
 * @author Volodymyr Velykyi
 */

public class Application {
    /**
     * Interval is boundaries for our odd and even numbers.
     */
    private static int[] interval = new int[2];

    /**
     * Ask user enter an interval.
     * Print sequence of odd and even numbers and their sums.
     * Ask user enter a count of Fibonacci numbers.
     * Print numbers and the biggest odd and even numbers from the sequence.
     *
     * @param args is parameter of method.
     */
    public static void main(String[] args) {
        int n;
        int maxOdd;
        int maxEven;
        int[] fibonacciNumbers;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter begin and end of your interval.");
        System.out.print("Begin: ");
        interval[0] = scanner.nextInt();
        System.out.print("End: ");
        interval[1] = scanner.nextInt();
        maxOdd = Number.printOdd(interval);
        maxEven = Number.printEven(interval);
        System.out.print("Enter count of Fibonacci numbers: ");
        n = scanner.nextInt();
        if (maxEven > maxOdd) {
            fibonacciNumbers = Fibonacci.createNumbers(n, maxOdd, maxEven);
        } else {
            fibonacciNumbers = Fibonacci.createNumbers(n, maxEven, maxOdd);
        }
        Fibonacci.printBiggestOdd(fibonacciNumbers);
        Fibonacci.printBiggestEven(fibonacciNumbers);
        Fibonacci.countPercentage(fibonacciNumbers);
    }
}
